import React,{useState, useEffect} from 'react';
import Card from '../components/Card';
import mobilUtama from '../images/img_car.png' 
import SearchTab from '../components/SearchTab';
import { useNavigate } from "react-router-dom";


const HomePage = () => {

    const navigate = useNavigate();


    return (
        <>
            <div class="container-fluid bg-light" >
                <div
                    class="row pt-5 d-flex align-items-center page-1"
                >
                    <div class="col-lg-6 area-text0 pb-5 pt-4 " style={{paddingLeft: "10vw"}}>
                        <h2 class=" fw-bold">
                            Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)
                        </h2>
                        <div class="">
                            Selamat datang di Binar Car Rental. Kami menyediakan mobil
                            kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                            kebutuhanmu untuk sewa mobil selama 24 jam.
                        </div>
                        <button type="button" class="btn btn-success" onClick={()=>navigate("/")}>
                            Kembali ke Halaman Utama
                        </button>
                    </div>
                    <div class="col-lg-6 area-gambar0 pt-5 px-0">
                        <div class="gambar0 d-flex">
                            <img class="gambar1" src={mobilUtama} alt="" />
                        </div>
                    </div>
                </div>
            </div>
            
            <SearchTab />
            <Card/>
            

            {}

        </>
    );
}

export default HomePage;
