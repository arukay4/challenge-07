import React,{useState, useEffect} from 'react';
import mobilUtama from '../images/img_car.png' 
import { useNavigate } from "react-router-dom";

const HomeUtama = () => {
    
    const navigate = useNavigate();
    return (
        <>
            <div class="container-fluid bg-light" >
                <div
                    class="row pt-5 d-flex align-items-center page-1"
                >
                    <div class="col-lg-6 area-text0 pb-5 pt-4 " style={{paddingLeft: "10vw"}}>
                        <h2 class=" fw-bold">
                            Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)
                        </h2>
                        <div class="">
                            Selamat datang di Binar Car Rental. Kami menyediakan mobil
                            kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                            kebutuhanmu untuk sewa mobil selama 24 jam.
                        </div>
                        
                        <button type="button" class="btn btn-success" onClick={()=>navigate("/cars")}>
                            Mulai Sewa Mobil
                        </button>
                    </div>
                    <div class="col-lg-6 area-gambar0 pt-5 px-0">
                        <div class="gambar0 d-flex">
                            <img class="gambar1" src={mobilUtama} alt="" />
                        </div>
                    </div>
                </div>
            </div>
            
        
            <div class="container-fluid">
                <div class="row pt-5 d-flex align-items-center" id="ourservice">
                    <div class="col-lg-6 area-gambar0">
                        <img src="/images/img_service.png" class="gambar2" alt="" />

                    </div>
                    <div class="col-lg-6 area-text0 px-4 py-5">
                        <h3 class="me-5 pe-5 mb-4">
                            Best Car Rental for any kind of trip in (Lokasimu)!
                        </h3>
                        <div class="pe-lg-5 me-lg-5">
                            Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
                            lebih murah dibandingkan yang lain, kondisi mobil baru, serta
                            kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
                            wedding, meeting, dll.
                        </div>
                        <ul>
                            <li class="mb-2">
                                <img src="/images/icon_ceklis.png" class="pe-2" alt="" />

                                Sewa Mobil Dengan Supir di Bali 12 Jam
                            </li>
                            <li class="mb-2">
                                <img src="/images/icon_ceklis.png" class="pe-2" alt="" />
                                Sewa Mobil Lepas Kunci di Bali 24 Jam
                            </li>
                            <li class="mb-2">
                                <img src="/images/icon_ceklis.png" class="pe-2" alt="" />
                                Sewa Mobil Jangka Panjang Bulanan
                            </li>
                            <li class="mb-2">
                                <img src="/images/icon_ceklis.png" class="pe-2" alt="" />
                                Gratis Antar - Jemput Mobil di Bandara
                            </li>
                            <li class="mb-2">
                                <img src="/images/icon_ceklis.png" class="pe-2" alt="" />
                                Layanan Airport Transfer / Drop In Out
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class=" area-why0 pt-lg-5" id="whyus">
                <div class="row px-0">
                    <h3>Why Us?</h3>
                    <h6 class="text-muted">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </h6>
                </div>
                <div class="row">
                    <div class="area-kartu0">
                        <div class="shadow bg-body rounded-3 kartuWhyUs" style={{width: "18rem"}}>
                            <div class="card-body">
                                <h5 class="card-title mb-4">
                                    <img src="/images/jempol_kuning.png" class="pe-2" alt="" />
                                </h5>
                                <h4 class="card-subtitle mb-2">Mobil Lengkap</h4>
                                <p class="card-text text-muted">
                                    Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                                    terawat.
                                </p>
                            </div>
                        </div>
                        <div class="shadow bg-body rounded-3 kartuWhyUs" style={{width: "18rem"}}>
                            <div class="card-body">
                                <h5 class="card-title mb-4">
                                    <img src="/images/tag_merah.png" class="pe-2" alt="" />
                                </h5>
                                <h4 class="card-subtitle mb-2">Harga Murah</h4>
                                <p class="card-text text-muted">
                                    Harga murah dan bersaing, bisa bandingkan harga kami dengan
                                    rental mobil lain
                                </p>
                            </div>
                        </div>
                        <div class="shadow bg-body rounded-3 kartuWhyUs" style={{width: "18rem"}}>
                            <div class="card-body">
                                <h5 class="card-title mb-4">
                                    <img src="/images/professional_hijau.png" class="pe-2" alt="" />

                                </h5>
                                <h4 class="card-subtitle mb-2">Layanan 24 Jam</h4>
                                <p class="card-text text-muted">
                                    Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga
                                    tersedia di akhir minggu
                                </p>
                            </div>
                        </div>
                        <div class="shadow bg-body rounded-3 kartuWhyUs" style={{width: "18rem"}}>
                            <div class="card-body">
                                <h5 class="card-title mb-4">
                                    <img src="/images/jam_biru.png" class="pe-2" alt="" />
                                </h5>
                                <h4 class="card-subtitle mb-2">Sopir Profesional</h4>
                                <p class="card-text text-muted">
                                    Sopir yang profesional, berpengalaman, jujur, ramah dan selalu
                                    tepat waktu
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid pt-5 py-lg-5 mb-lg-5">
                <div class="row area-testi0">
                    <h3>Testimonials</h3>
                    <h6>berbagai review produk kami</h6>
                </div>
                <div
                    id="carouselExampleControls"
                    class="row carousel slide"
                    data-bs-ride="carousel"
                >
                    <div class="carousel-inner">
                        <div class="carousel-item active">

                            <div class="row">
                                <div
                                    class="col-lg-4 d-flex justify-content-center align-items-center"
                                >
                                    <div class="card shadow" style={{width: "30rem"}}>
                                        <div class="row">
                                            <div
                                                class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                            >
                                                <img src="/images/foto2.png" class="pe-2" alt="" />

                                            </div>
                                            <div class="col-lg-8 col-12">
                                                <div class="card-body d-flex flex-column  py-4">
                                                    <div class="d-flex justify-content-center justify-content-lg-start">
                                                        <img
                                                            src="./images/rate_kuning.png"
                                                            alt=""
                                                            class="pb-2 "
                                                        />
                                                    </div>

                                                    <p class="card-text">
                                                        Some quick example text to build on the card title
                                                        and make up the bulk of the card's content.
                                                    </p>
                                                    <h6>John Dee 32, Bromo</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div
                                    class="col-lg-4 d-flex justify-content-center align-items-center"
                                >
                                    <div class="card shadow d-none d-lg-block" style={{width: "30rem"}}>
                                        <div class="row">
                                            <div
                                                class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                            >

                                                <img src="/images/foto2.png" class="pe-2" alt="" />
                                            </div>
                                            <div class="col-lg-8 col-12">
                                                <div class="card-body d-flex flex-column  py-4">
                                                    <div class="d-flex justify-content-center justify-content-lg-start">
                                                        <img
                                                            src="./images/rate_kuning.png"
                                                            alt=""
                                                            class="pb-2 w-25 "
                                                        />
                                                    </div>

                                                    <p class="card-text">
                                                        Some quick example text to build on the card title
                                                        and make up the bulk of the card's content.
                                                    </p>
                                                    <h6>John Dee 32, Bromo</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div
                                    class="col-lg-4 d-flex justify-content-center align-items-center"
                                >
                                    <div class="card shadow d-none d-lg-block" style={{width: "30rem"}}>
                                        <div class="row">
                                            <div
                                                class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                            >
                                                <img src="/images/foto2.png" class="pe-2" alt="" />

                                            </div>
                                            <div class="col-lg-8 col-12">
                                                <div class="card-body d-flex flex-column  py-4">
                                                    <div class="d-flex justify-content-center justify-content-lg-start">
                                                        <img
                                                            src="/images/rate_kuning.png"
                                                            alt=""
                                                            class="pb-2 w-25 "
                                                        />
                                                    </div>

                                                    <p class="card-text">
                                                        Some quick example text to build on the card title
                                                        and make up the bulk of the card's content.
                                                    </p>
                                                    <h6>John Dee 32, Bromo</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item ">

                            <div class="row">
                                <div
                                    class="col-lg-4 d-flex justify-content-center align-items-center"
                                >
                                    <div class="card shadow" style={{width: "30rem"}}>
                                        <div class="row">
                                            <div
                                                class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                            >
                                                <img src="/images/foto2.png" class="pe-2" alt="" />

                                            </div>
                                            <div class="col-lg-8 col-12">
                                                <div class="card-body d-flex flex-column  py-4">
                                                    <div class="d-flex justify-content-center justify-content-lg-start">
                                                        <img
                                                            src="/images/rate_kuning.png"
                                                            alt=""
                                                            class="pb-2 "
                                                        />
                                                    </div>

                                                    <p class="card-text">
                                                        Some quick example text to build on the card title
                                                        and make up the bulk of the card's content.
                                                    </p>
                                                    <h6>John Dee 32, Bromo</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div
                                    class="col-lg-4 d-flex justify-content-center align-items-center"
                                >
                                    <div class="card shadow d-none d-lg-block" style={{width: "30rem"}}>
                                        <div class="row">
                                            <div
                                                class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                            >
                                                <img src="/images/foto2.png" class="pe-2" alt="" />

                                            </div>
                                            <div class="col-lg-8 col-12">
                                                <div class="card-body d-flex flex-column  py-4">
                                                    <div class="d-flex justify-content-center justify-content-lg-start">
                                                        <img
                                                            src="/images/rate_kuning.png"
                                                            alt=""
                                                            class="pb-2 w-25 "
                                                        />
                                                    </div>

                                                    <p class="card-text">
                                                        Some quick example text to build on the card title
                                                        and make up the bulk of the card's content.
                                                    </p>
                                                    <h6>John Dee 32, Bromo</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div
                                    class="col-lg-4 d-flex justify-content-center align-items-center"
                                >
                                    <div class="card shadow d-none d-lg-block" style={{width: "30rem"}}>
                                        <div class="row">
                                            <div
                                                class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                            >

                                                <img src="/images/foto2.png" class="pe-2" alt="" />

                                            </div>
                                            <div class="col-lg-8 col-12">
                                                <div class="card-body d-flex flex-column  py-4">
                                                    <div class="d-flex justify-content-center justify-content-lg-start">
                                                        <img
                                                            src="/images/rate_kuning.png"
                                                            alt=""
                                                            class="pb-2 w-25 "
                                                        />
                                                    </div>

                                                    <p class="card-text">
                                                        Some quick example text to build on the card title
                                                        and make up the bulk of the card's content.
                                                    </p>
                                                    <h6>John Dee 32, Bromo</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="btn-karosel pt-0">
                <button
                    class="carousel-control"
                    type="button"
                    data-bs-target="#carouselExampleControls"
                    data-bs-slide="prev"
                >
                    <img src="./images/Left_button.png" alt="" />
                </button>
                <button
                    class="carousel-control"
                    type="button"
                    data-bs-target="#carouselExampleControls"
                    data-bs-slide="next"
                >
                    <img src="./images/Right_button.png" alt="" />
                </button>
            </div>
            <div class="row g-0">
                <div class="col-12">
                    <div class="square">
                        <div class="area-text1">
                            <h1 class="">Sewa Mobil di (Lokasimu) Sekarang</h1>
                            <div class="py-3 mx-auto mb-4">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                Quibusdam rem commodi reiciendis nihil dolore esse
                            </div>
                            <button type="button" class="btn btn-success">
                                Mulai Sewa Mobil
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div class="container-fluid" id="faq">
                    <div class="row">
                        <div class="col-lg-5 area-faq1 pt-2">
                            <h2 class="mb-4 fw-bold">Frequently Asked Question</h2>
                            <div>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div class="col-lg-7 area-faq1">
                            <div class="">
                                <div class="accordion" id="accordionExample">
                                    <div class="accordion-item my-3 rounded">
                                        <h2 class="accordion-header" id="headingOne">
                                            <button
                                                class="accordion-button collapsed rounded"
                                                type="button"
                                                data-bs-toggle="collapse"
                                                data-bs-target="#collapseOne"
                                                aria-expanded="true"
                                                aria-controls="collapseOne"
                                            >
                                                Apa saja syarat yang dibutuhkan?
                                            </button>
                                        </h2>
                                        <div
                                            id="collapseOne"
                                            class="accordion-collapse collapse"
                                            aria-labelledby="headingOne"
                                            data-bs-parent="#accordionExample"
                                        >
                                            <div class="accordion-body">
                                                <strong
                                                >This is the first item's accordion body.</strong
                                                >
                                                It is shown by default, until the collapse plugin adds
                                                the appropriate classes that we use to style each
                                                element. These classes control the overall appearance,
                                                as well as the showing and hiding via CSS transitions.
                                                You can modify any of this with custom CSS or overriding
                                                our default variables. It's also worth noting that just
                                                about any HTML can go within the
                                                <code>.accordion-body</code>, though the transition does
                                                limit overflow.
                                            </div>
                                        </div>
                                    </div>

                                    <div class="accordion-item my-3 border-top rounded">
                                        <h2 class="accordion-header rounded" id="headingTwo">
                                            <button
                                                class="accordion-button collapsed rounded"
                                                type="button"
                                                data-bs-toggle="collapse"
                                                data-bs-target="#collapseTwo"
                                                aria-expanded="false"
                                                aria-controls="collapseTwo"
                                            >
                                                Berapa hari minimal sewa mobil lepas kunci?
                                            </button>
                                        </h2>
                                        <div
                                            id="collapseTwo"
                                            class="accordion-collapse collapse"
                                            aria-labelledby="headingTwo"
                                            data-bs-parent="#accordionExample"
                                        >
                                            <div class="accordion-body">
                                                <strong
                                                >This is the second item's accordion body.</strong
                                                >
                                                It is hidden by default, until the collapse plugin adds
                                                the appropriate classes that we use to style each
                                                element. These classes control the overall appearance,
                                                as well as the showing and hiding via CSS transitions.
                                                You can modify any of this with custom CSS or overriding
                                                our default variables. It's also worth noting that just
                                                about any HTML can go within the
                                                <code>.accordion-body</code>, though the transition does
                                                limit overflow.
                                            </div>
                                        </div>
                                    </div>

                                    <div class="accordion-item my-3 border-top rounded">
                                        <h2 class="accordion-header rounded" id="headingThree">
                                            <button
                                                class="accordion-button collapsed rounded"
                                                type="button"
                                                data-bs-toggle="collapse"
                                                data-bs-target="#collapseThree"
                                                aria-expanded="false"
                                                aria-controls="collapseThree"
                                            >
                                                Berapa hari sebelumnya sabaiknya booking sewa mobil?
                                            </button>
                                        </h2>
                                        <div
                                            id="collapseThree"
                                            class="accordion-collapse collapse"
                                            aria-labelledby="headingThree"
                                            data-bs-parent="#accordionExample"
                                        >
                                            <div class="accordion-body">
                                                <strong
                                                >This is the second item's accordion body.</strong
                                                >
                                                It is hidden by default, until the collapse plugin adds
                                                the appropriate classes that we use to style each
                                                element. These classes control the overall appearance,
                                                as well as the showing and hiding via CSS transitions.
                                                You can modify any of this with custom CSS or overriding
                                                our default variables. It's also worth noting that just
                                                about any HTML can go within the
                                                <code>.accordion-body</code>, though the transition does
                                                limit overflow.
                                            </div>
                                        </div>
                                    </div>

                                    <div class="accordion-item my-3 border-top rounded">
                                        <h2 class="accordion-header rounded" id="headingFour">
                                            <button
                                                class="accordion-button collapsed rounded"
                                                type="button"
                                                data-bs-toggle="collapse"
                                                data-bs-target="#collapseFour"
                                                aria-expanded="false"
                                                aria-controls="collapseFour"
                                            >
                                                Apakah Ada biaya antar-jemput?
                                            </button>
                                        </h2>
                                        <div
                                            id="collapseFour"
                                            class="accordion-collapse collapse"
                                            aria-labelledby="headingFour"
                                            data-bs-parent="#accordionExample"
                                        >
                                            <div class="accordion-body">
                                                <strong
                                                >This is the second item's accordion body.</strong
                                                >
                                                It is hidden by default, until the collapse plugin adds
                                                the appropriate classes that we use to style each
                                                element. These classes control the overall appearance,
                                                as well as the showing and hiding via CSS transitions.
                                                You can modify any of this with custom CSS or overriding
                                                our default variables. It's also worth noting that just
                                                about any HTML can go within the
                                                <code>.accordion-body</code>, though the transition does
                                                limit overflow.
                                            </div>
                                        </div>
                                    </div>

                                    <div class="accordion-item my-3 border-top rounded">
                                        <h2 class="accordion-header rounded" id="headingFive">
                                            <button
                                                class="accordion-button collapsed rounded"
                                                type="button"
                                                data-bs-toggle="collapse"
                                                data-bs-target="#collapseFive"
                                                aria-expanded="false"
                                                aria-controls="collapseFive"
                                            >
                                                Bagaimana jika terjadi kecelakaan
                                            </button>
                                        </h2>
                                        <div
                                            id="collapseFive"
                                            class="accordion-collapse collapse"
                                            aria-labelledby="headingFive"
                                            data-bs-parent="#accordionExample"
                                        >
                                            <div class="accordion-body">
                                                <strong
                                                >This is the second item's accordion body.</strong
                                                >
                                                It is hidden by default, until the collapse plugin adds
                                                the appropriate classes that we use to style each
                                                element. These classes control the overall appearance,
                                                as well as the showing and hiding via CSS transitions.
                                                You can modify any of this with custom CSS or overriding
                                                our default variables. It's also worth noting that just
                                                about any HTML can go within the
                                                <code>.accordion-body</code>, though the transition does
                                                limit overflow.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    );
}

export default HomeUtama;
