import { Routes, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css' 
import 'bootstrap/dist/js/bootstrap.bundle.js' 
import HomeUtama from "./pages/HomeUtama";
import HomePage from "./pages/HomePage"
import './pages/style.css'
import Footer from './components/Footer'
import Navbar from "./components/Navbar"

function App() {
  return (
<div className="App">
  <Navbar />
    <Routes>
      <Route path='/' element={<HomeUtama />} />
      <Route path='/cars' element={<HomePage />} />
    </Routes>
  <Footer />
    </div>
  );
}

export default App;
