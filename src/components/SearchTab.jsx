import React,{useState} from 'react';
import { useSelector, useDispatch } from "react-redux";
import {filter} from "../store/actions/FilterActions"


const SearchTab = () => {
    const dispatch = useDispatch();
    // const filter = useSelector((state) => state.filter)

    const [driver, setDriver] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [passenger, setPassenger] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        let data = {
            driver: driver,
            date: date,
            time: time,
            passenger: passenger
        }
        console.log(data);
        dispatch(filter(data))
    }

    console.log(driver, date, time, passenger);

    return (
        <div>
            <section id="hero" class="hero">
                <div class="container">
                <div id="floating-form" class="floating-form">
                    <div class="container">
                    <div class="card shadow p-3">
                        <div class="card-body">
                        <div class="form-wrapper">
                            <div class="form-field">
                            <label  class="form-label" >Tipe Driver</label>
                            <select class="form-select" name="driver" id="tipeDriver" style={{color: "gray;"}} onChange={(e) => setDriver(e.target.value)} >
                                <option value="" hidden class="text-muted" >Pilih Tipe Driver</option>
                                <option value={true} style={{color:" black !important;"}}>Dengan Sopir</option>
                                <option value={false} style={{color:" black !important;"}}>Tanpa Sopir (Lepas Kunci) </option>
                            </select>
                            </div>
                            <div class="form-field">
                            <label class="form-label">Tanggal</label>
                            <input type="date" class="form-control" name="date" id="dateSewa" placeholder="Pilih Tanggal" onfocus="(this.type='date')" onblur="(this.type='text')" onChange={(e) => setDate(e.target.value)} />
                            </div>
                            <div class="form-field">
                            <label for="time" class="form-label">Waktu Jemput/Ambil</label>
                            <select class="form-select" name="passengers" id="waktuJemput" style={{color: "gray;"}} onChange={(e) => setTime(e.target.value)} >
                                <option value="" hidden>Pilih Waktu</option>
                                <option value="8" style={{color:" black !important;"}}>08.00 WIB</option>
                                <option value='9' style={{color:" black !important;"}}>09.00 WIB</option>
                                <option value='10' style={{color:" black !important;"}}>10.00 WIB</option>
                                <option value='11' style={{color:" black !important;"}}>11.00 WIB</option>
                            </select>
                            </div>
                            <div class="form-field">
                            <label for="passengers" class="form-label">Jumlah Penumpang(optional)</label>
                            <input type="number" class="form-control" name="passengers" id="jumlahPenumpang" placeholder="Jumlah Penumpang" min="1" max="6" onChange={(e) => setPassenger(e.target.value)}/>
                            
                            </div>
                            <div class="d-flex align-items-end">
                            <button type="button" class="btn btn-success" id="submitFilter" onClick={(e) => handleSubmit(e)}>Cari Mobil</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </section>
        </div>
    );
}

export default SearchTab;
