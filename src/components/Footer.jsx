import React from 'react';

const Footer = () => {
    return (
        <div>
            <div class="container-fluid">
                <div class="row pb-5 ps-lg-5 pe-lg-5">
                    <div class="col-md-3 p-md-5 pt-5">
                        <p>Jalan Suroyo No. 161 Mayangan Kota Probolongo 672000</p>
                        <p>binarcarrental@gmail.com</p>
                        <p>081-233-334-808</p>
                    </div>
                    <div class="col-md-3 p-md-5 h6" >
                        <p class="ps-md-5 ">
                            <a href="#ourservice" class="text-decoration-none text-body">Our Service</a>
                        </p>
                        <p class="ps-md-5 ">
                            <a href="#whyus" class="text-decoration-none text-body">Why Us</a>
                        </p>
                        <p class="ps-md-5 ">
                            <a href="#testi" class="text-decoration-none text-body">Testimonials</a>
                        </p>
                        <p class="ps-md-5 ">
                            <a href="#faq" class="text-decoration-none text-body">FAQ</a>
                        </p>
                    </div>
                    <div class="col-md-3  pt-md-5">
                        <p>Connect with us</p>
                        <div class="d-flex flex-wrap justify-content-between col-10 col-md-12  align-items-around pe-5 pe-md-0 me-5 me-md-0 pb-2" >
                            <img src="/images/icon_facebook.png" class="pe-2" alt="" />
                            <img src="/images/icon_instagram.png" class="pe-2" alt="" />
                            <img src="/images/icon_twitter.png" class="pe-2" alt="" />
                            <img src="/images/icon_mail.png" class="pe-2" alt="" />
                            <img src="/images/icon_twitch.png" class="pe-2" alt="" />
                        </div>
                    </div>
                    <div class="col-md-3 p-md-5">
                        <p>Copyright Binar 2022</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;
