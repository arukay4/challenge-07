import React,{useState, useEffect} from 'react';
import axios from 'axios'
import { useSelector } from "react-redux";
import iconUser from '../images/iconUser.png'
import iconSetting from '../images/iconSetting.png'
import iconCalender from '../images/iconCalender.png'
import reloadCat from '../images/reloadss.gif'

const Card = () => {

    const filterMasihDalamArray = useSelector((state) => state.filter); // filter ini pas diambil dari store masih bentuknya array dalam array, makanay di console.log nya filter[0]
    const filter = filterMasihDalamArray[0];
    console.log(filterMasihDalamArray, "ini filter yang diambil dari store")

    const [data, setData] = useState([])

    useEffect(() => {
        // async jgn di useEffect, sumber : https://devtrium.com/posts/async-functions-useeffect#:~:text=Translated%20with%20a%20setState%20function%2C%20it%20looks%20like%20this%3A
        const fetchData = async () => {
            const resp = await axios.get('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json');
            const hasilSlice = resp.data.slice(0,3)
            setData(...data,resp.data)
            console.log(resp.data, "ini data") // kok ga muncul ya ?
        }
        fetchData()
        // .catch(console.error)
      }, []);


    return (
        <>
        {(data.length === 0) ? 
            <h1 className='d-flex justify-content-center'>
                <img src={reloadCat} alt="" />
            </h1> 
            :
            <>
            {(filterMasihDalamArray.length === 0) ? 
            <>
            <div style={{padding: "0 10vw"}}>
                <div className="row">
                {data.map((isi, index) => {
                    
                return      <div className="col-4 pb-4">
                                <div class="card shadow h-100">
                                    <div class="card-body">
                                        <img src={`${"https://res.cloudinary.com/adiwee/image/upload/v1653654765"}`+ `${(isi.image).substring(1)}`} class="img-fluid" style={{height: "300px",objectFit: "cover", width: "100%"}} />
                                        <h5 class="car-title pt-2">{isi.manufacture +" "+ isi.model}</h5>
                                        <h2 class="car-price">Rp {isi.rentPerDay} / hari </h2>
                                        <p class="car-subtitle">{isi.description}</p>
                                    </div>
                                    <div class="car-detail px-3 pb-3">
                                        <div class="detail-item pb-2"> 
                                            <img src={iconUser} alt=""  className='pe-2'/>
                                            {isi.capacity}
                                        </div>
                                        <div class="detail-item pb-2">
                                            <img src={iconSetting} alt="" className='pe-2' />
                                            {isi.transmission}
                                        </div>
                                        <div class="detail-item pb-2">
                                            <img src={iconCalender} alt="" className='pe-2' />
                                            {isi.year}
                                        </div>
                                    </div>
                                    <div class="px-3 pb-3">
                                        <button type="button" class="btn btn-success w-100" style={{position:"relative", bottom:"0px"}}>Pilih Mobil</button>
                                    </div>
                                </div>
                            </div>
                })}

                    </div>
                </div>
            </>
            :
            <>
                <div style={{padding: "0 10vw"}}>
                <div className="row">
                {filter.map((isi) => {
                    
                return      <div className="col-4 pb-4">
                                <div class="card shadow h-100">
                                    <div class="card-body">
                                        <img src={`${"https://res.cloudinary.com/jansencloud1/image/upload/v1653569960"}`+ `${(isi.image).substring(1)}`} class="img-fluid" style={{height: "300px",objectFit: "cover", width: "100%"}} />
                                        <h5 class="car-title pt-2">{isi.manufacture +" "+ isi.model}</h5>
                                        <h2 class="car-price">Rp {isi.rentPerDay} / hari </h2>
                                        <p class="car-subtitle">{isi.description}</p>
                                    </div>
                                    <div class="car-detail px-3 pb-3">
                                        <div class="detail-item pb-2"> 
                                            <img src={iconUser} alt=""  className='pe-2'/>
                                            {isi.capacity}
                                        </div>
                                        <div class="detail-item pb-2">
                                            <img src={iconSetting} alt="" className='pe-2' />
                                            {isi.transmission}
                                        </div>
                                        <div class="detail-item pb-2">
                                            <img src={iconCalender} alt="" className='pe-2' />
                                            {isi.year}
                                        </div>
                                    </div>
                                    <div class="px-3 pb-3">
                                        <button type="button" class="btn btn-success w-100" style={{position:"relative", bottom:"0px"}}>Pilih Mobil</button>
                                    </div>
                                </div>
                            </div>
                })}

                    </div>
                </div>
            </>
            
            
            }
            
            </>

            
        }
        </>

    )
      
}

export default Card;
