# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Cloudinary 

Pada project ini saya pakai cloudinary supaya bisa ambil gambar yang ada di db, soalnya di db itu imagenya tidak url melainkan seperti path local info. Gambarnya saya ambil dari file di chapter 4.